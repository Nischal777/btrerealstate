from django.shortcuts import render, redirect
from django.contrib import messages
from .models import Contact
from django.core.mail import send_mail

# Create your views here.
def contact(request):
    if request.method == 'POST':
        listing_id = request.POST['listing_id']
        listing = request.POST['listing']
        name = request.POST['name']
        email = request.POST['email']
        phone = request.POST['phone']
        message = request.POST['message']
        user_id = request.POST['user_id']
        realtor_email = request.POST['realtor_email']

        #checking is user has made an inquiry alreay

        if request.user.is_authenticated:
            user_id = request.user.id
            has_contacted = Contact.objects.filter(listing_id=listing_id, user_id=user_id)
            if has_contacted:
                messages.error(request, 'already submitted inquiry')
                return redirect('/listings/'+listing_id)

        contact = Contact(listing=listing, listing_id=listing_id, user_id=user_id, name=name,email=email,phone=phone, message=message)
        contact.save()

        #sending Mail
        send_mail(
            'Property Listing Inquiry',
            'There has been as inquiry for' + listing +'. Sign into the admin pannel for more info',
            'nischaljoshi77@gmail.com',
            [realtor_email,'nischaljoshi77@gmail.com'],
            fail_silently=False

        )
        messages.success(request, 'Your Request has been submitted')
        return redirect('/listings/'+listing_id)

